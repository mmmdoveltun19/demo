package az.ingress.demo;

import az.ingress.demo.model.Student;
import az.ingress.demo.repository.*;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class SpringDemoMs19Application implements CommandLineRunner {

    private final StudentRepository studentRepository;
    private final BalanceRepository balanceRepository;
    private final PhoneRepository phoneRepository;
    private final RoleRepository roleRepository;
    private final EntityManagerFactory entityManagerFactory;

    public static void main(String[] args) {
        SpringApplication.run(SpringDemoMs19Application.class, args);
    }


    @Override
//    @Transactional
    public void run(String... args) throws Exception {
        List<Student> all = studentRepository.findAllBy();
        all.forEach(System.out::println);

        Student student = studentRepository.findById(5).get();

        System.out.println(student.getAge());

//        studentRepository.findByName("name_8e48c1a5a77a")
//                .ifPresent(System.out::println);

//		EntityManager entityManager = entityManagerFactory.createEntityManager();
//		entityManager.createNamedQuery("Student_AGE_EQUALS",Student.class).getResultList();


    }
}