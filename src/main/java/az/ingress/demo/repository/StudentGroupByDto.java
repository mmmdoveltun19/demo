package az.ingress.demo.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public interface StudentGroupByDto {
    Integer getAge();
    Long getCount();
}
